# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.5'

gem 'mysql2', '~> 0.5.2'
gem 'puma', '~> 4.2', '>= 4.2.1'
gem 'rails', '6.0.2.1'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

gem 'devise', '~> 4.7', '>= 4.7.1'
gem 'devise-jwt', '~> 0.6.0'

gem 'graphql', '~> 1.9', '>= 1.9.14'
gem 'rack-cors', '~> 1.0', '>= 1.0.3'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]

  gem 'factory_bot_rails', '~> 5.1', '>= 5.1.1'
  gem 'rspec-rails', '~> 4.0.0.beta3'
end

group :development do
  gem 'annotate', '~> 3.0', '>= 3.0.3'
  gem 'awesome_print', '~> 1.8'
  gem 'rubocop', '~> 0.75.1'

  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'simplecov', '~> 0.17.1'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
