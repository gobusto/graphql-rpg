GraphQL RPG Server
==================

This is an API-only Rails 6 project which uses Devise-JWT and GraphQL-RB as the
basis for a user-interfaceless Role Playing Game, with the idea being to create
one or more "clients" in order to display the state of the game in an arbitrary
way. These clients might use a simple "text adventure" interface, or a top-down
Final Fantasy VI-style map, or a Kingdom Of Loathing-style browser GUI, or even
a card-game-type display - the goal is to let the client present things however
it wants to!

Note that this is currently just an experiment I work on in my spare time; it's
nowhere near complete yet!

How it works
------------

When you log into (or create) an account, an `Authorization` header is included
in the HTTP response. Send this header back to the server when making requests;
it'll be used to identify who the request came from.

<https://github.com/waiting-for-dev/devise-jwt>

Once you have this, send requests to the `/graphql` endpoint to get information
about things (queries), or to perform actions (mutations).

<https://graphql-ruby.org/>

In the future, you'll also be able to access real-time updates (subscriptions),
but this requires me to sort out ActionCable WebSockets first...

TODO
----

Game features:

+ Factions (AKA guilds/clans).
    + Allow members to deposit/withdraw money.
    + Allow members to store items.
    + Allow leaders to destroy the faction.
    + Automatically process "pending" memberships if the member-policy changes.
    + There should also be a "noticeboard" or some other way to communicate.
    + Having "furniture" that members can use in some way would be nice too.
+ Consider how locations (and movement between them) could work.
    + A "Kingdom of Loathing" approach is simplest, but...
    + Is there a better way to have "structured" areas?
+ Finish implementing battles:
  + Fully implement skills/health/etc. so that battles can work properly.
  + Allow players to leave battles when they win/lose/escape.
  + Limit the circumstances for starting PVP/co-op battles somehow.
  + Limit which monsters can appear at which times (especially as allies!).
  + Maybe add a time limit for turns, in order to prevent players from idling?
  + Allow monsters to use/drop items.
+ Flesh out stat increases; this will be similar to how Dark Souls levels work.
+ Implement equipment (allow items to be added to character slots).
+ Implement shops, so that players can buy (or sell) items.

Technical features:

+ Implement GraphQL-via-Websockets for real-time updates.
+ Sort out pagination/filtering/etc. for GraphQL "collection" fields.
+ Use the `graphql-batch` Gem.
+ Sort out password-reset emails.
+ Sort out account locking.
+ Sort out changing emails/passwords.
+ Allow (some!) user information to be queried via GraphQL.
    + Exclude their email address from this - unless a user queries themself.
    + This allows users to have "profile" pages with links to Twitter/etc.

Hacking on the server
---------------------

It's just a Rails project, so the usual `rails s`, `rails c` stuff applies.

A specific version of Ruby is defined in both `Gemfile` and `.ruby-version`, so
you may wish to consider using RVM: <https://rvm.io/>

+ Use `rspec` to run tests.
+ Use `./list_badly_named_specs.sh` to find any mis-named RSpec test files.
+ Use `rubocop` to lint the code.

**TODO:** Write something about generating a new `config/master.key` file here.

(There's nothing in `config/credentials.yml.enc` other than `secret_key_base`.)

Building Clients
----------------

This section is mainly just notes for now... see the examples in the `clients/`
subdirectory, or refer to the tests in `spec/requests/` for further details.

Note that you can also query the GraphQL schema via introspection:

<https://graphql.org/learn/introspection/>

Create a new user:

    curl -v -XPOST http://127.0.0.1:3000/users --data 'user[email]=someone@gmail.com' --data 'user[password]=password'

Sign in as an existing user:

    curl -v -XPOST http://127.0.0.1:3000/users/sign_in --data 'user[email]=someone@gmail.com' --data 'user[password]=password'

Run a GraphQL query as a particular user, via an access token:

    curl -v -XPOST http://127.0.0.1:3000/graphql --data 'query={ currentUser { email } }' --header 'Authorization: Bearer ey...5I'

Sign out:

    curl -v -XDELETE http://127.0.0.1:3000/users/sign_out --header 'Authorization: Bearer ey...5I'

**tl;dr:** It's the usual Devise REST endpoints, but as JSON, plus `/graphql`.

Licence
-------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
licence, allowing you to use or modify it for any purpose, including commercial
and closed-source projects. All I ask in return is that _proper attribution_ is
given (i.e. don't remove my name from the copyright text in the source code and
perhaps include me on the "credits" screen, if your program has such a thing).
