# frozen_string_literal: true

module ApplicationCable
  # This is the base channel class, from which all others are derived.
  class Channel < ActionCable::Channel::Base
  end
end
