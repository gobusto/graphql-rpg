# frozen_string_literal: true

module ApplicationCable
  # This is the base connection class, from which all others are derived.
  class Connection < ActionCable::Connection::Base
  end
end
