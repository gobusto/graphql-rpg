# frozen_string_literal: true

# This is the base controller class, from which all others are derived.
class ApplicationController < ActionController::API
  include ActionController::MimeResponds # For Devise JWT.

  respond_to :json # For Devise JWT.
end
