# frozen_string_literal: true

module Mutations
  # This class is used as a parent for all mutations.
  class BaseMutation < GraphQL::Schema::Mutation
    field :errors, [String], null: false # All mutations have this.

    private

    def current_user
      context[:current_user]
    end

    def success(field_name, value)
      { 'errors' => [], field_name => value }
    end

    def failure(field_name, errors)
      { 'errors' => Array(errors), field_name => nil }
    end
  end
end
