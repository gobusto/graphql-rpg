# frozen_string_literal: true

module Mutations
  # Create a character.
  class CreateCharacter < BaseMutation
    argument :attributes, Types::CharacterAttributes, required: true

    field :character, Types::CharacterType, null: true

    def resolve(attributes:)
      character = Character.new(attributes.to_h.merge(user: current_user))
      return success(:character, character) if character.save

      failure(:character, character.errors.full_messages)
    end
  end
end
