# frozen_string_literal: true

module Mutations
  # Create a faction.
  class CreateFaction < BaseMutation
    argument :character_id, GraphQL::Types::ID, required: true
    argument :attributes, Types::FactionAttributes, required: true

    field :faction, Types::FactionType, null: true

    def resolve(character_id:, attributes:)
      faction = Factions::Instance.new(attributes.to_h)

      faction.faction_memberships.build(
        character: current_user.characters.find_by(id: character_id),
        status: :leader
      )

      return success(:faction, faction) if faction.save

      failure(:faction, faction.errors.full_messages)
    end
  end
end
