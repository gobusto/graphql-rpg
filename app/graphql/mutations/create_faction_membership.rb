# frozen_string_literal: true

module Mutations
  # Join a faction.
  class CreateFactionMembership < BaseMutation
    argument :faction_id, GraphQL::Types::ID, required: true
    argument :character_id, GraphQL::Types::ID, required: true

    field :faction_membership, Types::FactionMembershipType, null: true

    def resolve(faction_id:, character_id:)
      membership = build_faction_membership(faction_id, character_id)
      return success(:faction_membership, membership) if membership.save

      failure(:faction_membership, membership.errors.full_messages)
    end

    private

    def build_faction_membership(id, character_id)
      whom = current_user.characters.find_by(id: character_id)
      membership = Factions::Membership.new(faction_id: id, character: whom)
      membership.status = :pending if membership.faction&.approval_based?
      membership.status = :standard if membership.faction&.unrestricted?
      membership
    end
  end
end
