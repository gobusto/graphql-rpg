# frozen_string_literal: true

module Mutations
  # Destroy a character.
  class DestroyCharacter < BaseMutation
    argument :id, GraphQL::Types::ID, required: true

    field :character, Types::CharacterType, null: true

    def resolve(id:)
      character = current_user.characters.find_by(id: id)
      return failure(:character, 'Record not found') unless character
      return success(:character, character) if character.destroy

      failure(:character, character.errors.full_messages)
    end
  end
end
