# frozen_string_literal: true

module Mutations
  # Leave the current faction.
  class DestroyFactionMembership < BaseMutation
    argument :id, GraphQL::Types::ID, required: true

    field :character, Types::CharacterType, null: true

    def resolve(id:)
      model = current_user.faction_memberships.find_by(id: id)
      return failure(:character, 'You do not belong to a faction') unless model
      return failure(:character, 'No other leaders') if only_leader?(model)
      return success(:character, model.character) if model.destroy

      failure(:character, model.errors.full_messages)
    end

    private

    def only_leader?(model)
      model.leader? && model.faction.faction_memberships.leader.count == 1
    end
  end
end
