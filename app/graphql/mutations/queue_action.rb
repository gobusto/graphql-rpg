# frozen_string_literal: true

module Mutations
  # Queue a battle action.
  class QueueAction < BaseMutation
    argument :character_id, GraphQL::Types::ID, required: true
    argument :skill_id, GraphQL::Types::ID, required: true

    field :battle, Types::BattleType, null: true

    def resolve(character_id:, skill_id:)
      whom = current_user.characters.find_by(id: character_id)
      what = whom&.skills&.find_by(id: skill_id)

      action = Battles::Action.new(involvement: whom&.involvement, skill: what)
      return success(:battle, action.involvement.instance) if action.save

      failure(:battle, action.errors.full_messages)
    end
  end
end
