# frozen_string_literal: true

module Mutations
  # Start a new battle.
  class StartBattle < BaseMutation
    argument :involvements, [Types::InvolvementAttributes], required: true

    field :battle, Types::BattleType, null: true

    def resolve(involvements:)
      params = involvements.as_json
      battle = Battles::Instance.new(involvements_attributes: params)
      return success(:battle, battle) if battle.save

      failure(:battle, battle.errors.full_messages)
    end
  end
end
