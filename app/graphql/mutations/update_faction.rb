# frozen_string_literal: true

module Mutations
  # Update a faction.
  class UpdateFaction < BaseMutation
    argument :id, GraphQL::Types::ID, required: true
    argument :attributes, Types::FactionAttributes, required: true

    field :faction, Types::FactionType, null: true

    def resolve(id:, attributes:)
      membership = current_user.faction_memberships.find_by(faction_id: id)
      return failure(:faction, 'No faction membership found') unless membership
      return failure(:faction, 'You are not a leader') unless membership.leader?

      faction = membership.faction
      return success(:faction, faction) if faction.update(attributes.to_h)

      failure(:faction, faction.errors.full_messages)
    end
  end
end
