# frozen_string_literal: true

module Mutations
  # Update the status of a faction membership.
  class UpdateFactionMembership < BaseMutation
    argument :id, GraphQL::Types::ID, required: true
    argument :status, Types::MembershipStatus, required: true

    field :faction_membership, Types::FactionMembershipType, null: true

    def resolve(id:, status:)
      membership = Factions::Memberships.find_by(id: id)
      return failure(:faction_membership, 'Record not found') unless membership

      my = current_user.faction_memberships.leader.find_by(
        faction_id: membership.faction_id
      )
      return failure(:faction_membership, 'Not a leader') unless my

      membership.status = status
      return success(:faction_membership, membership) if membership.save

      failure(:faction_membership, membership.errors.full_messages)
    end
  end
end
