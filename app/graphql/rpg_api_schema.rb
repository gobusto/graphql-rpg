# frozen_string_literal: true

# This schema allows queries and mutations, but not subscriptions.
class RpgApiSchema < GraphQL::Schema
  mutation(Types::MutationType)
  query(Types::QueryType)
end
