# frozen_string_literal: true

module Types
  # Auto-generated...
  class BaseObject < GraphQL::Schema::Object
    field_class Types::BaseField
  end
end
