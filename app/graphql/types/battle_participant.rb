# frozen_string_literal: true

module Types
  # Used to represent a generic battle participant, regardless of type.
  module BattleParticipant
    include Types::BaseInterface
    include Types::DatabaseRecord

    description 'A generic battle participant'

    field :name, String, null: false

    %i[money experience].each { |f| field f, Int, null: false }
    %i[strength speed vitality].each { |f| field f, Int, null: false }
    %i[magic faith personality].each { |f| field f, Int, null: false }
    %i[current_health current_mana].each { |f| field f, Int, null: false }
    %i[maximum_health maximum_mana].each { |f| field f, Int, null: false }

    field :involvement, InvolvementType, null: true
    field :skills, [Types::SkillType], null: false

    orphan_types Types::CharacterType, Types::MonsterType

    definition_methods do
      def resolve_type(object, _context)
        object.is_a?(Character) ? Types::CharacterType : Types::MonsterType
      end
    end
  end
end
