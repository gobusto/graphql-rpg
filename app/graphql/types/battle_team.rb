# frozen_string_literal: true

module Types
  # Enumerates possible teams for battles.
  class BattleTeam < Types::BaseEnum
    Battles::Involvement.teams.each do |value, _|
      value(value.upcase, "The #{value} team", value: value)
    end
  end
end
