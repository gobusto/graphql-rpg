# frozen_string_literal: true

module Types
  # This represents a battle.
  class BattleType < Types::BaseObject
    implements Types::DatabaseRecord

    field :involvements, [InvolvementType], null: false
  end
end
