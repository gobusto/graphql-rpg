# frozen_string_literal: true

module Types
  # Attributes used to create a new character.
  class CharacterAttributes < Types::BaseInputObject
    description 'Attributes used to create a new character'

    argument :name, String, 'The character name', required: true
  end
end
