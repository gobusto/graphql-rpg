# frozen_string_literal: true

module Types
  # This represents a character.
  class CharacterType < Types::BaseObject
    implements Types::DatabaseRecord
    implements Types::BattleParticipant

    field :faction_membership, FactionMembershipType, null: true
    field :items, [Types::ItemType], null: false
  end
end
