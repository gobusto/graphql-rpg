# frozen_string_literal: true

module Types
  # A general interface shared by all database-backed models.
  module DatabaseRecord
    include Types::BaseInterface

    description 'A general database record'

    field :id, GraphQL::Types::ID, 'Database ID', null: false
    field :model_name, String, 'Model type', null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, 'Created', null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, 'Updated', null: false
  end
end
