# frozen_string_literal: true

module Types
  # Attributes used to create or update a faction.
  class FactionAttributes < Types::BaseInputObject
    description 'Attributes used to create or update a faction'

    argument :name, String, 'The faction name', required: false

    argument(
      :membership_policy,
      MembershipPolicy,
      'The faction name',
      required: false
    )
  end
end
