# frozen_string_literal: true

module Types
  # This links a character to a faction.
  class FactionMembershipType < Types::BaseObject
    implements Types::DatabaseRecord

    field :character, CharacterType, null: false
    field :faction, FactionType, null: false
    field :status, MembershipStatus, null: false
  end
end
