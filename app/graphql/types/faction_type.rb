# frozen_string_literal: true

module Types
  # This represents a faction.
  class FactionType < Types::BaseObject
    implements Types::DatabaseRecord

    field :name, String, null: false
    field :money, Int, null: false
    field :membership_policy, MembershipPolicy, null: false
    field :faction_memberships, [FactionMembershipType], null: false
  end
end
