# frozen_string_literal: true

module Types
  # Attributes used to create a new battle participant.
  class InvolvementAttributes < Types::BaseInputObject
    description 'Attributes used to create a new battle participant'

    argument :team, BattleTeam, 'Which team are they on?', required: false
    argument :participant_type, ParticipantKind, 'Player/NPC?', required: true
    argument :participant_id, GraphQL::Types::ID, 'Database ID', required: true
  end
end
