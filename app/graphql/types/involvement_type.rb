# frozen_string_literal: true

module Types
  # This represents a battle-involvement.
  class InvolvementType < Types::BaseObject
    implements Types::DatabaseRecord

    field :instance, BattleType, null: false
    field :participant, BattleParticipant, null: false
    field :team, BattleTeam, null: true
    field :ready, Boolean, null: false

    def ready
      object.next_action.present?
    end
  end
end
