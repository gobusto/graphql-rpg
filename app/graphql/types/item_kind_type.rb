# frozen_string_literal: true

module Types
  # This represents an item type.
  class ItemKindType < Types::BaseObject
    implements Types::DatabaseRecord

    field :name, String, null: false
  end
end
