# frozen_string_literal: true

module Types
  # This represents an item held by a character.
  class ItemType < Types::BaseObject
    implements Types::DatabaseRecord

    field :kind, Types::ItemKindType, null: false
  end
end
