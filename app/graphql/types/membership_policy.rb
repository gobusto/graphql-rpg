# frozen_string_literal: true

module Types
  # Enumerates possible membership-application policies for factions.
  class MembershipPolicy < Types::BaseEnum
    value(
      'INVITE_ONLY',
      'Applications are closed',
      value: 'invite_only'
    )
    value(
      'APPROVAL_BASED',
      'Applications are open, but subject to approval from an existing member',
      value: 'approval_based'
    )
    value(
      'UNRESTRICTED',
      'Applications are automatically accepted',
      value: 'unrestricted'
    )
  end
end
