# frozen_string_literal: true

module Types
  # Enumerates possible membership types for factions.
  class MembershipStatus < Types::BaseEnum
    value(
      'STANDARD',
      'The character is a regular member of the faction',
      value: 'standard'
    )
    value(
      'PENDING',
      'A membership application has been made, but not yet approved',
      value: 'pending'
    )
    value(
      'LEADER',
      'The character is a special member, and can approve new members',
      value: 'leader'
    )
  end
end
