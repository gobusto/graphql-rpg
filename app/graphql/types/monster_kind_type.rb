# frozen_string_literal: true

module Types
  # This represents a monster type.
  class MonsterKindType < Types::BaseObject
    implements Types::DatabaseRecord

    field :name, String, null: false
  end
end
