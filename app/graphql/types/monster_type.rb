# frozen_string_literal: true

module Types
  # This represents a monster instance.
  class MonsterType < Types::BaseObject
    implements Types::DatabaseRecord
    implements Types::BattleParticipant

    field :kind, MonsterKindType, null: false
  end
end
