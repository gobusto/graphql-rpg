# frozen_string_literal: true

module Types
  # This is the "entry point" for mutations.
  class MutationType < Types::BaseObject
    CATEGORIES = {
      action: %i[queue],
      battle: %i[start],
      character: %i[create destroy],
      faction: %i[create update],
      faction_membership: %i[create update destroy]
    }.freeze

    CATEGORIES.each do |noun, verbs|
      verbs.each do |verb|
        action = "#{verb}_#{noun}"
        field "#{verb}_#{noun}", mutation: Mutations.const_get(action.camelize)
      end
    end
  end
end
