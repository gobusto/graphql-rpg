# frozen_string_literal: true

module Types
  # Enumerates possible participant types for battle involvements.
  class ParticipantKind < Types::BaseEnum
    value 'CHARACTER', 'A player character', value: 'Character'
    value 'MONSTER', 'A monster NPC', value: 'Monsters::Kind'
  end
end
