# frozen_string_literal: true

module Types
  # This is the "entry point" for queries.
  class QueryType < Types::BaseObject
    field :current_user, UserType, null: false, description: 'The current user'

    field :factions, [FactionType], null: false, description: 'All factions'
    field :items, [ItemKindType], null: false, description: 'All items'
    field :monsters, [MonsterKindType], null: false, description: 'All monsters'

    def current_user
      context[:current_user]
    end

    def factions
      Factions::Instance.all
    end

    def items
      Items::Kind.all
    end

    def monsters
      Monsters::Kind.all
    end
  end
end
