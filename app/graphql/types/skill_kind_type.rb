# frozen_string_literal: true

module Types
  # This represents a skill type.
  class SkillKindType < Types::BaseObject
    implements Types::DatabaseRecord

    field :name, String, null: false
  end
end
