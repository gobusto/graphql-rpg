# frozen_string_literal: true

module Types
  # This represents a skill known by a character/NPC.
  class SkillType < Types::BaseObject
    implements Types::DatabaseRecord

    field :kind, Types::SkillKindType, null: false
  end
end
