# frozen_string_literal: true

module Types
  # This represents a user.
  class UserType < Types::BaseObject
    implements Types::DatabaseRecord

    field :email, String, null: false
    field :characters, [Types::CharacterType], null: false
  end
end
