# frozen_string_literal: true

# This is the base mailer class, from which all others are derived.
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
