# frozen_string_literal: true

# This is the base model class, from which all others are derived.
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  private

  def add_error(field, reason)
    model = self.class.table_name.singularize.split('_').join('.')
    error = "activerecord.errors.models.#{model}.attributes.#{field}.#{reason}"
    errors[field] << I18n.t(error)
  end
end
