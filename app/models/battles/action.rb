# frozen_string_literal: true

# == Schema Information
#
# Table name: battles_actions
#
#  id             :bigint           not null, primary key
#  resolved       :boolean          default(FALSE), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  involvement_id :bigint
#  skill_id       :bigint
#
# Indexes
#
#  index_battles_actions_on_involvement_id  (involvement_id)
#  index_battles_actions_on_skill_id        (skill_id)
#

module Battles
  # This represents the action taken on a given round.
  class Action < Base
    belongs_to :involvement
    belongs_to :skill, class_name: 'Skills::Instance'

    validate :verify_new_actions_allowed!, on: :create, if: :involvement

    # New actions must be unresolved, and only one may be "queued" at any time:
    validates :resolved, absence: true, on: :create
    validates :resolved, uniqueness: { scope: :involvement_id }, on: :create

    # The only time we ever update an existing action is if we're resolving it:
    validates :resolved, acceptance: true, on: :update

    # If this action was specified by a player, check whether we are ready now:
    after_create_commit :update_battle_state!

    private

    def verify_new_actions_allowed!
      add_error(:participant, :dead) if involvement.dead?
      add_error(:battle, :complete) if involvement.instance&.complete?
    end

    # Resolving a turn means creating monster actions, so avoid recursion here:
    def update_battle_state!
      involvement.instance.resolve! if involvement.participant.is_a?(Character)
    end
  end
end
