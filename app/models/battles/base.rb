# frozen_string_literal: true

module Battles
  # Base class for this namespace.
  class Base < ApplicationRecord
    self.abstract_class = true

    def self.table_name_prefix
      'battles_'
    end
  end
end
