# frozen_string_literal: true

# == Schema Information
#
# Table name: battles_instances
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Battles
  # This represents a battle instance.
  class Instance < Base
    has_many :involvements, dependent: :destroy
    accepts_nested_attributes_for :involvements, allow_destroy: true
    validates :team_count, numericality: { greater_than: 1 }, on: :create

    # NOTE: Each "unaligned" participant is effectively their own team.
    def team_count
      actual_teams = active_involvements.map(&:team).compact.uniq
      independents = active_involvements.reject(&:team?)
      actual_teams.count + independents.count
    end

    def complete?
      team_count < 2 # Everyone is dead, or only one "side" remains active.
    end

    def resolve!
      return false if complete? || !players_ready?

      transaction do
        update_monsters!
        resolve_actions!
      end

      true
    end

    private

    def active_involvements
      involvements.reject(&:dead?)
    end

    def active_players
      active_involvements.select { |the| the.participant.is_a?(Character) }
    end

    def active_monsters
      active_involvements.reject { |the| the.participant.is_a?(Character) }
    end

    def unresolved_actions
      involvements.map(&:next_action).compact
    end

    def players_ready?
      active_players.count == unresolved_actions.count # No NPC actions yet!
    end

    # TODO: Implement a better "strategy" than this...
    def update_monsters!
      active_monsters.each do |npc|
        npc.actions.create!(skill: npc.participant.skills.first)
      end
    end

    # TODO: Sort by participant speed, so that faster ones go first.
    def resolve_actions!
      unresolved_actions.each do |action|
        # action.execute unless action.involvement.dead?
        action.update!(resolved: true)
      end
    end
  end
end
