# frozen_string_literal: true

# == Schema Information
#
# Table name: battles_involvements
#
#  id               :bigint           not null, primary key
#  participant_type :string(255)
#  team             :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  instance_id      :bigint
#  participant_id   :bigint
#
# Indexes
#
#  index_battles_involvements_on_instance_id  (instance_id)
#  index_battles_involvements_on_participant  (participant_type,participant_id) UNIQUE
#

module Battles
  # This links a character or NPC to a battle instance.
  class Involvement < Base
    enum team: { red: 0, blue: 1, green: 2, yellow: 3 } # `nil` = No team.

    belongs_to :instance
    belongs_to :participant, polymorphic: true
    has_many :actions, dependent: :destroy

    before_validation :create_instance_of_type, on: :create

    validates :participant_id, uniqueness: { scope: :participant_type }
    validates :participant_type, inclusion: %w[Character Monsters::Instance]

    validates :dead?, absence: true, on: :create # Can't fight if you're dead!

    def dead?
      participant.respond_to?(:current_health) && participant.current_health < 1
    end

    def next_action
      actions.find_by(resolved: false) # May be `nil` if no action is queued.
    end

    private

    def create_instance_of_type
      return unless participant.is_a?(Monsters::Kind)

      self.participant = Monsters::Instance.new(kind: participant)
    end
  end
end
