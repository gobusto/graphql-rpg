# frozen_string_literal: true

# == Schema Information
#
# Table name: characters
#
#  id             :bigint           not null, primary key
#  current_health :integer          default(0), not null
#  current_mana   :integer          default(0), not null
#  experience     :integer          default(0), not null
#  faith          :integer          default(0), not null
#  magic          :integer          default(0), not null
#  money          :integer          default(0), not null
#  name           :string(255)
#  personality    :integer          default(0), not null
#  speed          :integer          default(0), not null
#  strength       :integer          default(0), not null
#  vitality       :integer          default(0), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint
#
# Indexes
#
#  index_characters_on_name     (name) UNIQUE
#  index_characters_on_user_id  (user_id)
#

# This represents a character associated with an individual user.
class Character < ApplicationRecord
  include CharacterOrMonster
  include Battles::Participant

  belongs_to :user
  has_one(
    :faction_membership,
    class_name: 'Factions::Membership',
    inverse_of: :character,
    dependent: :destroy
  )
  has_many(
    :items,
    class_name: 'Items::Instance',
    inverse_of: :character,
    dependent: :destroy
  )
end
