# frozen_string_literal: true

module Battles
  # Logic shared by both `Character` and `Monsters::Instance` models.
  module Participant
    extend ActiveSupport::Concern

    included do
      has_one :involvement, class_name: 'Battles::Involvement', as: :participant

      after_initialize :refill_health, if: :new_record?
      after_initialize :refill_mana, if: :new_record?
      before_validation :cap_health_and_mana
    end

    def refill_health
      self.current_health = maximum_health.to_i # nil if no `Monsters::Kind`
    end

    def refill_mana
      self.current_mana = maximum_mana.to_i # nil if no `Monsters::Kind`
    end

    private

    def cap_health_and_mana
      self.current_health = [0, [current_health, maximum_health.to_i].min].max
      self.current_mana = [0, [current_mana, maximum_mana.to_i].min].max
    end
  end
end
