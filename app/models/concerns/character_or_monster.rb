# frozen_string_literal: true

# Logic shared by both `Character` and `Monsters::Kind` models.
module CharacterOrMonster
  extend ActiveSupport::Concern

  included do
    has_many(
      :skills,
      class_name: 'Skills::Instance',
      as: :caster,
      dependent: :destroy
    )

    validates :name, presence: true, uniqueness: { case_sensitive: false }

    %i[strength speed vitality magic faith personality].each do |f|
      validates f, numericality: { greater_than_or_equal_to: 0 }
    end

    %i[experience money].each do |f|
      validates f, numericality: { greater_than_or_equal_to: 0 }
    end
  end

  def maximum_health
    10 * (vitality + 1)
  end

  def maximum_mana
    10 * (magic + faith)
  end
end
