# frozen_string_literal: true

module Factions
  # Base class for this namespace.
  class Base < ApplicationRecord
    self.abstract_class = true

    def self.table_name_prefix
      'factions_'
    end
  end
end
