# frozen_string_literal: true

# == Schema Information
#
# Table name: factions_instances
#
#  id                :bigint           not null, primary key
#  membership_policy :integer          default("invite_only"), not null
#  money             :integer          default(0), not null
#  name              :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_factions_instances_on_name  (name) UNIQUE
#

module Factions
  # This represents a faction.
  class Instance < Base
    enum membership_policy: {
      invite_only: 0,
      approval_based: 1,
      unrestricted: 2
    }

    has_many(
      :faction_memberships,
      class_name: 'Factions::Membership',
      inverse_of: :faction,
      foreign_key: :faction_id,
      dependent: :destroy
    )

    validates :name, presence: true, uniqueness: { case_sensitive: false }
    validates :membership_policy, presence: true
    validates :money, numericality: { greater_than_or_equal_to: 0 }
  end
end
