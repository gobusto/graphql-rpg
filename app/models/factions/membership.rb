# frozen_string_literal: true

# == Schema Information
#
# Table name: factions_memberships
#
#  id           :bigint           not null, primary key
#  status       :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  character_id :bigint
#  faction_id   :bigint
#
# Indexes
#
#  index_factions_memberships_on_character_id  (character_id) UNIQUE
#  index_factions_memberships_on_faction_id    (faction_id)
#

module Factions
  # This links a character with a faction.
  class Membership < Base
    enum status: { standard: 0, pending: 1, leader: 2 }

    belongs_to :character
    belongs_to :faction, class_name: 'Factions::Instance'

    validates :character, uniqueness: true
    validates :status, presence: true
  end
end
