# frozen_string_literal: true

# == Schema Information
#
# Table name: invalidated_jwts
#
#  id  :bigint           not null, primary key
#  exp :datetime         not null
#  jti :string(255)      not null
#
# Indexes
#
#  index_invalidated_jwts_on_jti  (jti)
#

# See https://github.com/waiting-for-dev/devise-jwt
class InvalidatedJwt < ApplicationRecord
  include Devise::JWT::RevocationStrategies::Blacklist
end
