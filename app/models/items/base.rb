# frozen_string_literal: true

module Items
  # Base class for this namespace.
  class Base < ApplicationRecord
    self.abstract_class = true

    def self.table_name_prefix
      'items_'
    end
  end
end
