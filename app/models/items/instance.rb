# frozen_string_literal: true

# == Schema Information
#
# Table name: items_instances
#
#  id           :bigint           not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  character_id :bigint
#  kind_id      :bigint
#
# Indexes
#
#  index_items_instances_on_character_id  (character_id)
#  index_items_instances_on_kind_id       (kind_id)
#

module Items
  # This represents an item held by a character.
  class Instance < Base
    belongs_to :character
    belongs_to :kind
  end
end
