# frozen_string_literal: true

# == Schema Information
#
# Table name: items_kinds
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_items_kinds_on_name  (name) UNIQUE
#

module Items
  # This represents a possible type of item.
  class Kind < Base
    validates :name, presence: true, uniqueness: { case_sensitive: false }

    has_many :instances, dependent: :destroy
  end
end
