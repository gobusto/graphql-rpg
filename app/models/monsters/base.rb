# frozen_string_literal: true

module Monsters
  # Base class for this namespace.
  class Base < ApplicationRecord
    self.abstract_class = true

    def self.table_name_prefix
      'monsters_'
    end
  end
end
