# frozen_string_literal: true

# == Schema Information
#
# Table name: monsters_instances
#
#  id             :bigint           not null, primary key
#  current_health :integer          default(0), not null
#  current_mana   :integer          default(0), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  kind_id        :bigint
#
# Indexes
#
#  index_monsters_instances_on_kind_id  (kind_id)
#

module Monsters
  # This represents an individual instance of a monster.
  class Instance < Base
    include Battles::Participant

    belongs_to :kind
    has_many :skills, through: :kind

    delegate :name, :money, :experience, to: :kind, allow_nil: true
    delegate :strength, :speed, :vitality, to: :kind, allow_nil: true
    delegate :magic, :faith, :personality, to: :kind, allow_nil: true
    delegate :maximum_health, :maximum_mana, to: :kind, allow_nil: true
  end
end
