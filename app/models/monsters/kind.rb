# frozen_string_literal: true

# == Schema Information
#
# Table name: monsters_kinds
#
#  id          :bigint           not null, primary key
#  experience  :integer          default(0), not null
#  faith       :integer          default(0), not null
#  magic       :integer          default(0), not null
#  money       :integer          default(0), not null
#  name        :string(255)
#  personality :integer          default(0), not null
#  speed       :integer          default(0), not null
#  strength    :integer          default(0), not null
#  vitality    :integer          default(0), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_monsters_kinds_on_name  (name) UNIQUE
#

module Monsters
  # This represents a possible type of monster.
  class Kind < Base
    include CharacterOrMonster

    has_many :instances
  end
end
