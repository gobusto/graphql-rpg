# frozen_string_literal: true

module Skills
  # Base class for this namespace.
  class Base < ApplicationRecord
    self.abstract_class = true

    def self.table_name_prefix
      'skills_'
    end
  end
end
