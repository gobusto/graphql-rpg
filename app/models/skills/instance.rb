# frozen_string_literal: true

# == Schema Information
#
# Table name: skills_instances
#
#  id          :bigint           not null, primary key
#  caster_type :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  caster_id   :bigint
#  kind_id     :bigint
#
# Indexes
#
#  index_skills_instances_on_caster_type_and_caster_id  (caster_type,caster_id)
#  index_skills_instances_on_kind_id                    (kind_id)
#

module Skills
  # This links a skill to a character or monster instance.
  class Instance < Base
    belongs_to :kind
    belongs_to :caster, polymorphic: true

    # validates :kind_id, uniqueness: { scope: %i[caster_type caster_id] }
    validates :caster_type, inclusion: %w[Character Monsters::Kind]

    # Determines chance-of-use for NPCs (or "auto-battle" characters)
    # validates :chance, numericality: { greater: 0 }
  end
end
