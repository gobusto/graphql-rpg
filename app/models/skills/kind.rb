# frozen_string_literal: true

module Skills
  # This defines a character or NPC skill.
  class Kind < Base
    validates :name, presence: true, uniqueness: { case_sensitive: false }
  end
end
