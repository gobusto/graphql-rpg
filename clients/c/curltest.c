/* gcc curltest.c -o curltest -lcurl */

#include <curl/curl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define HTTP_SERVER "http://127.0.0.1:3000"

static char authorization[256];

/* https://curl.haxx.se/libcurl/c/CURLOPT_HEADERFUNCTION.html */
static size_t authFunc(char *data, size_t size, size_t items, void *userdata) {
  const size_t n = size * items;
  if (n > 22 && n < 255 && memcmp(data, "Authorization: Bearer ", 22) == 0) {
    for (size_t i = 0; i < n; ++i) {
      authorization[i] = (data[i] == '\r' || data[i] == '\n') ? 0 : data[i];
    }
  }
  return n;
}

/* https://curl.haxx.se/libcurl/c/CURLOPT_WRITEFUNCTION.html */
static size_t nullFunc(char *data, size_t size, size_t items, void *userdata) {
  return size * items;
}

static void httpClearToken(void) {
  memset(authorization, 0, 256);
}

static int httpHaveToken(void) {
  return authorization[0]; /* If this is not NULL, then we're authenticated. */
}

static int httpInit(void) {
  httpClearToken();
  return curl_global_init(CURL_GLOBAL_ALL);
}

static void httpQuit(void) {
  httpClearToken();
  curl_global_cleanup();
}

/* Exchange an email/password pair for an access token: */
static int httpSignIn(const char *email, const char *password) {
  CURL *curl;
  CURLcode result;

  /* FIXME: Construct this string from the supplied params! */
  const char *fields = "user[email]=test@example.com&user[password]=password";

  httpClearToken();

  if (!email || !password) { return -1; }
  curl = curl_easy_init();
  if (!curl) { return 1; }

  curl_easy_setopt(curl, CURLOPT_URL, HTTP_SERVER "/users/sign_in");
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, fields);
  curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, authFunc);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, nullFunc);
  result = curl_easy_perform(curl);
  curl_easy_cleanup(curl);

  return result == CURLE_OK && httpHaveToken() ? 0 : 666;
}

/* Execute a GraphQL query, authenticating with our current access token: */
static int httpGraphQL(const char *query, const char *params) {
  CURL *curl;
  CURLcode result;
  struct curl_slist *header = NULL;

  /* FIXME: Construct this string from the supplied params! */
  const char *fields = "query={currentUser{email}}";

  if (!httpHaveToken() || !query) { return -1; }
  curl = curl_easy_init();
  if (!curl) { return 1; }

  header = curl_slist_append(header, authorization);

  curl_easy_setopt(curl, CURLOPT_URL, HTTP_SERVER "/graphql");
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, fields);
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header);
  /* curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, jsonFunc); */
  result = curl_easy_perform(curl);
  curl_easy_cleanup(curl);

  return result == CURLE_OK ? 0 : 666;
}

int main(int argc, char **argv) {
  if (httpInit() != 0) { return 1; }

  if (httpSignIn("test@example.com", "password") == 0) {
    printf("Success: `%s`\n", authorization);
  } else {
    printf("Invalid Email or password.\n");
  }

  if (httpGraphQL("{ currentUser { email } }", NULL) == 0) {
    printf(" - GraphQL query executed successfully.\n");
  } else {
    printf(" - Couldn't execute GraphQL query.\n");
  }

  httpQuit();

  return 0;
}
