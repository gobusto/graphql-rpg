GraphQL-RPG: ReactJS client.
============================

This is a simple web client, used to test the back-end server. It's just a HTML
page - no Node/NPM/etc. - so there's no pre-processing or JSX here.

NOTE: Use <https://nagoshiashumari.github.io/Rpg-Awesome/> at some point...
