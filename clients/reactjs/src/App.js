class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = { token: null }

    this.handleToken = this.handleToken.bind(this)
  }

  handleToken (token) {
    this.setState({ token: token })
  }

  render () {
    const e = React.createElement

    if (!this.state.token) {
      return e(LandingPage, { onLogIn: this.handleToken })
    }

    return e(Session, { token: this.state.token, onLogOut: this.handleToken })
  }
}
