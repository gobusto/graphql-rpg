/*
This file contains various API functions related to user account management.

User accounts are handled by Devise, so errors may be in one of two formats:

    { "error": "You need to sign in or sign up before continuing." }

...or as a Rails-style hash:

    {
      "errors": {
        "email": ["can't be blank"],
        "password": ["can't be blank"]
      }
    }

As such, you'll need to handle both options in any "reject" handlers you write.
*/

class AccountApi extends GenericApi {
  static create (email, password) {
    const params = { 'user[email]': email, 'user[password]': password }
    return this.send('post', 'users', null, params)
  }

  static destroy (token) {
    return this.send('delete', 'users', token)
  }

  static signIn (email, password) {
    const params = { 'user[email]': email, 'user[password]': password }
    return this.send('post', 'users/sign_in', null, params)
  }

  static signOut (token) {
    return this.send('delete', 'users/sign_out', token)
  }

  static forgotPassword (email) {
    return this.send('post', 'users/password', null, { 'user[email]': email })
  }

  static changePassword (code, password) {
    const params = {
      'user[reset_password_token]': code,
      'user[password]': password
    }
    return this.send('put', 'users/password', null, params)
  }
}
