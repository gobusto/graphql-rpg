class BattleApi extends GenericApi {
  // Start a new battle instance (one PC vs. one NPC)
  static start (characterId, monsterId) {
    const query = `
      mutation StartBattle($involvements: [InvolvementAttributes!]!) {
        startBattle(involvements: $involvements) {
          errors
          battle ${battleState}
        }
      }
    `

    const params = {
      attributes: {
        involvements: [
          {
            participantType: "CHARACTER",
            participantId: characterId,
            team: "RED"
          },
          {
            participantType: "MONSTER",
            participantId: monsterId,
            team: "BLUE"
          }
        ]
      }
    }

    return this.graphql(jwt, query, params, 'startBattle')
  }

  // Specify the next action a participant should take:
  static queueAction (characterId, skillId) {
    const query = `
      mutation QueueAction($characterId: ID!, $skillId: ID!) {
        queueAction(characterId: $characterId, skillId: $skillId) {
          errors
          battle ${battleState}
        }
      }
    `

    const params = {
      attributes: {
        characterId: characterId,
        skillId: skillId
      }
    }

    return this.graphql(jwt, query, params, 'queueAction')
  }
}

// Class-level constants (not sure if there's a nicer way to do this):
BattleApi.battleState = `
  {
    involvements {
      participant {
        name
        currentHealth
        maximumHealth
        currentMana
        maximumMana
      }
      team
    }
  }
`
