class CharacterApi extends GenericApi {
  // Fetch all characters belonging to the currently-signed-in user:
  static all (jwt) {
    const query = `
      {
        currentUser {
          characters {
            id,
            name,
            experience,
            money
          }
        }
      }
    `

    return this.graphql(jwt, query)
  }

  // Create a new character:
  static create (jwt, name) {
    const query = `
      mutation CreateCharacter($attributes: CharacterAttributes!) {
        createCharacter(attributes: $attributes) {
          errors,
          character {
            id,
            name,
            experience,
            money
          }
        }
      }
    `

    const params = {
      attributes: {
        name: name
      }
    }

    return this.graphql(jwt, query, params, 'createCharacter')
  }

  // Destroy an existing character:
  static destroy (jwt, id) {
    const query = `
      mutation DestroyCharacter($id: ID!) {
        destroyCharacter(id: $id) {
          errors
        }
      }
    `

    return this.graphql(jwt, query, { id: id }, 'destroyCharacter')
  }
}
