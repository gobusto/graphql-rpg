// This is basically just a nice wrapper around `XMLHttpRequest`.
class GenericApi {
  // Convert a set of key/value pairs into a `FormData` object for `send()`ing.
  static formify (params) {
    let data = new FormData()
    Object.keys(params || {}).forEach(key => data.append(key, params[key]))
    return data
  }

  // Send a request, get a response. Note that `jwt` and `params` are optional.
  static send (verb, path, jwt, params) {
    const server = 'http://127.0.0.1:3000/' // Change this if needed.

    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest()

      xhr.open(verb, server + path)
      if (jwt) { xhr.setRequestHeader('Authorization', jwt) }
      xhr.send(this.formify(params))

      xhr.onload = () => {
        // Errors always just pass back the JSON unchanged:
        const json = JSON.parse(xhr.responseText || 'null')
        if (xhr.status < 200 || xhr.status >= 300) { reject(json) }

        // If we made a non-authorised request (login, signup), return the JWT:
        const authorization = xhr.getResponseHeader('Authorization')
        resolve(jwt ? json : { 'authorization': authorization })
      }

      // Mimic { "error": "You need to sign in or sign up before continuing." }
      xhr.onerror = () => reject({ 'error': 'Could not connect to server.' })
      xhr.onabort = () => reject({ 'error': 'Request aborted.' })
      xhr.ontimeout = () => reject({ 'error': 'Request timed out.' })
    })
  }

  // For convenience, provide a simplified method for sending GraphQL requests:
  static graphql (jwt, query, params, mutation) {
    const data = { query: query, variables: JSON.stringify(params || {}) }

    return new Promise((resolve, reject) => {
      this.send('POST', 'graphql', jwt, data).then(
        json => {
          // Badly-formed GraphQL requests will have a root "errors" field:
          if (json.errors) { reject(json.errors.map(e => e.message)) }
          // Mutations (usually) include an errors array, but queries will not:
          const errors = mutation ? json.data[mutation].errors || [] : []
          // If data.mutationName.errors is not an empty array, it's a failure:
          errors.length ? reject(errors) : resolve(json.data)
        },
        // See { "error": "You need to sign in or sign up before continuing." }
        json => reject([json.error])
      )
    })
  }
}
