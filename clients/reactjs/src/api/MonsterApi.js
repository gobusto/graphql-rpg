class MonsterApi extends GenericApi {
  // Fetch a list of all monster archetypes (NOT instances):
  static all (jwt) {
    const query = `
      {
        monsters {
          id
          name
        }
      }
    `

    return this.graphql(jwt, query)
  }
}
