function Button (props) {
  const e = React.createElement

  return e(
    'button',
    { className: 'button', onClick: props.onClick },
    props.label
  )
}
