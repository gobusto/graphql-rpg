// A basic error message.
function ErrorMessage (props) {
  const e = React.createElement
  const text = props.text
  return text ? e('div', { className: 'error-message' }, text) : null
}
