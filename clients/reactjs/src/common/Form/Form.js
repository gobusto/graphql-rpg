function Form (props) {
  const e = React.createElement

  let cancelButton = null
  if (props.onCancel) {
    cancelButton = e(Button, { label: 'Cancel', onClick: props.onCancel })
  }

  return e(
    'form',
    { className: 'form', onSubmit: props.onSubmit },
    e('h1', { className: 'title' }, props.title),
    e(ErrorMessage, { text: props.error }),
    e('div', { className: 'body' }, props.children),
    e('div', { className: 'footer' },
      e(Button, { label: props.submitText || 'Save' }),
      cancelButton
    )
  )
}
