function LabelInput (props) {
  const e = React.createElement

  return e('div', { className: 'label-input' },
    e(
      'label',
      null,
      props.label
    ),
    e(
      'input',
      { onChange: props.onChange, type: props.type, value: props.value }
    )
  )
}
