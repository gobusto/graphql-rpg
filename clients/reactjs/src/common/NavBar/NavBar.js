function NavBar (props) {
  const e = React.createElement

  return e('div', { className: 'navbar' }, props.children)
}
