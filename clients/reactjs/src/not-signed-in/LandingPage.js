class LandingPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = { error: null }

    this.flatten = this.flatten.bind(this)

    this.handleSignUp = this.handleSignUp.bind(this)
    this.handleSignIn = this.handleSignIn.bind(this)

    this.handlePasswordEmail = this.handlePasswordEmail.bind(this)
    this.handlePasswordReset = this.handlePasswordReset.bind(this)
  }

  // Flatten a Rails-style errors object into a simple string:
  flatten (errors) {
    let result = []
    Object.keys(errors || {}).forEach(key => {
      errors[key].forEach(text => result.push([key, text].join(' ')))
    })
    return result.join(' / ')
  }

  handleSignUp (event, params) {
    event.preventDefault()

    if (params.password !== params.confirm) {
      return this.setState({ error: 'Password confirmation does not match' })
    }

    AccountApi.create(params.email, params.password).then(
      json => this.props.onLogIn(json.authorization),
      json => this.setState({ error: json.error || this.flatten(json.errors) })
    )
  }

  handleSignIn (event, params) {
    event.preventDefault()

    AccountApi.signIn(params.email, params.password).then(
      json => this.props.onLogIn(json.authorization),
      json => this.setState({ error: json.error })
    )
  }

  handlePasswordEmail (event, params) {
    event.preventDefault()

    AccountApi.forgotPassword(params.email).then(
      json => this.setState({ error: 'A password reset email has been sent' }),
      json => this.setState({ error: json.error || this.flatten(json.errors) })
    )
  }

  handlePasswordReset (event, params) {
    event.preventDefault()

    if (params.password !== params.confirm) {
      return this.setState({ error: 'Password confirmation does not match' })
    }

    AccountApi.changePassword(params.code, params.password).then(
      json => this.setState({ error: 'Your password has been reset' }),
      json => this.setState({ error: json.error || this.flatten(json.errors) })
    )
  }

  render () {
    const e = React.createElement

    return e('div', { className: 'landing-page' },
      e('h1', { className: 'logo' }, 'GraphQL RPG'),
      e(TabGroup, { labels: ['Log In', 'Create Account', 'Forgot Password', 'Reset Password'] },
        e(SignInForm, { onSubmit: this.handleSignIn, error: this.state.error }),
        e(SignUpForm, { onSubmit: this.handleSignUp, error: this.state.error }),
        e(PasswordEmailForm, { onSubmit: this.handlePasswordEmail, error: this.state.error }),
        e(PasswordResetForm, { onSubmit: this.handlePasswordReset, error: this.state.error })
      ),
      e('div', { className: 'footer' },
        'GQL-RPG web client ・ © 2019 ・ ',
        e(
          'a',
          { href: 'https://gitlab.com/gobusto/graphql-rpg/', target: '_blank' },
          'Source code'
        ),
        ' is available under the terms of the ',
        e(
          'a',
          { href: 'https://opensource.org/licenses/MIT', target: '_blank' },
          'MIT licence'
        )
      )
    )
  }
}
