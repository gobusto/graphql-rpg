class PasswordEmailForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = { email: '' }

    this.handleEmailChange = this.handleEmailChange.bind(this)
  }

  handleEmailChange (event) {
    this.setState({ email: event.target.value })
  }

  render () {
    const e = React.createElement

    return e(Form, {
      title: 'Forgot Password',
      error: this.props.error,
      submitText: 'Continue',
      onSubmit: event => this.props.onSubmit(event, this.state)
    },
      e(
        LabelInput,
        { label: 'Email', value: this.state.email, onChange: this.handleEmailChange, type: 'email' }
      )
    )
  }
}
