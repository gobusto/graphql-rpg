class PasswordResetForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = { code: '', password: '', confirm: '' }

    this.handleCodeChange = this.handleCodeChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.handleConfirmChange = this.handleConfirmChange.bind(this)
  }

  handleCodeChange (event) {
    this.setState({ code: event.target.value })
  }

  handlePasswordChange (event) {
    this.setState({ password: event.target.value })
  }

  handleConfirmChange (event) {
    this.setState({ confirm: event.target.value })
  }

  render () {
    const e = React.createElement

    return e(Form, {
      title: 'Reset Password',
      error: this.props.error,
      submitText: 'Continue',
      onSubmit: event => this.props.onSubmit(event, this.state)
    },
      e(
        LabelInput,
        { label: 'Code', value: this.state.code, onChange: this.handleCodeChange }
      ),
      e(
        LabelInput,
        { label: 'New Password', value: this.state.password, onChange: this.handlePasswordChange, type: 'password' }
      ),
      e(
        LabelInput,
        { label: 'Confirm', value: this.state.confirm, onChange: this.handleConfirmChange, type: 'password' }
      )
    )
  }
}
