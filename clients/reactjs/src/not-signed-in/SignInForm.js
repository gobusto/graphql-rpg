class SignInForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = { email: '', password: '' }

    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
  }

  handleEmailChange (event) {
    this.setState({ email: event.target.value })
  }

  handlePasswordChange (event) {
    this.setState({ password: event.target.value })
  }

  render () {
    const e = React.createElement

    return e(Form, {
      title: 'Log In',
      error: this.props.error,
      submitText: 'Continue',
      onSubmit: event => this.props.onSubmit(event, this.state)
    },
      e(
        LabelInput,
        { label: 'Email', value: this.state.email, onChange: this.handleEmailChange, type: 'email' }
      ),
      e(
        LabelInput,
        { label: 'Password', value: this.state.password, onChange: this.handlePasswordChange, type: 'password' }
      )
    )
  }
}
