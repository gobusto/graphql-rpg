class SignUpForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = { email: '', password: '', confirm: '' }

    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.handleConfirmChange = this.handleConfirmChange.bind(this)
  }

  handleEmailChange (event) {
    this.setState({ email: event.target.value })
  }

  handlePasswordChange (event) {
    this.setState({ password: event.target.value })
  }

  handleConfirmChange (event) {
    this.setState({ confirm: event.target.value })
  }

  render () {
    const e = React.createElement

    return e(Form, {
      title: 'Create Account',
      error: this.props.error,
      submitText: 'Continue',
      onSubmit: event => this.props.onSubmit(event, this.state)
    },
      e(
        LabelInput,
        { label: 'Email', value: this.state.email, onChange: this.handleEmailChange, type: 'email' }
      ),
      e(
        LabelInput,
        { label: 'Password', value: this.state.password, onChange: this.handlePasswordChange, type: 'password' }
      ),
      e(
        LabelInput,
        { label: 'Confirm', value: this.state.confirm, onChange: this.handleConfirmChange, type: 'password' }
      )
    )
  }
}
