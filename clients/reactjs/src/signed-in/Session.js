class Session extends React.Component {
  constructor (props) {
    super(props)
    this.state = { character: null }

    this.handleDestroyAccount = this.handleDestroyAccount.bind(this)
    this.handleLogOut = this.handleLogOut.bind(this)
    this.handleSelectCharacter = this.handleSelectCharacter.bind(this)
    this.handleDeselectCharacter = this.handleDeselectCharacter.bind(this)
  }

  handleDestroyAccount (event) {
    event.preventDefault()
    if (!window.confirm('Really delete your account?')) { return }

    AccountApi.destroy(this.props.token).then(
      json => this.props.onLogOut(),
      json => console.log(json)
    )
  }

  handleLogOut (event) {
    event.preventDefault()
    if (!window.confirm('Log out?')) { return }

    AccountApi.signOut(this.props.token).then(
      json => this.props.onLogOut(),
      json => console.log(json)
    )
  }

  handleSelectCharacter (event, character) {
    event.preventDefault()
    this.setState({ character: character })
  }

  handleDeselectCharacter (event) {
    event.preventDefault()
    this.setState({ character: null })
  }

  render () {
    const e = React.createElement

    if (!this.state.character) {
      return e(MainMenu, {
        token: this.props.token,
        onSelectCharacter: this.handleSelectCharacter,
        onDestroyAccount: this.handleDestroyAccount,
        onLogOut: this.handleLogOut
      })
    }

    return e(Game, {
      token: this.props.token,
      character: this.state.character,
      onDeselectCharacter: this.handleDeselectCharacter,
      onLogOut: this.handleLogOut
    })
  }
}
