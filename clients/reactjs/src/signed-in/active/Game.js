function Game (props) {
  const e = React.createElement

  return e('div', { className: 'game' },
    e(NavBar, null,
      e(Button, { label: 'Change character', onClick: props.onDeselectCharacter }),
      e(Button, { label: 'Log out', onClick: props.onLogOut })
    ),
    e('div', { className: 'content' },
      e('div', null, `You are currently playing as ${props.character.name}`),
      e('div', null, `Gold: ${props.character.money}`),
      e('div', null, `XP: ${props.character.experience}`)
    )
  )
}
