function AccountForm (props) {
  const e = React.createElement

  return e(Form, {
    title: 'Manage Account',
    submitText: 'Delete Account',
    onSubmit: props.onDestroyAccount
  },
    'You will be able to change your email or password here one day...'
  )
}
