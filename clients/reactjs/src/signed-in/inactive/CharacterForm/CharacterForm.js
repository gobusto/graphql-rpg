class CharacterForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = { name: '' }

    this.handleNameChange = this.handleNameChange.bind(this)
  }

  handleNameChange (event) {
    this.setState({ name: event.target.value })
  }

  render () {
    const e = React.createElement

    const onSubmit = event => this.props.onSubmit(event, this.state)
    return e(Form, {
      title: 'Create Character',
      error: this.props.error,
      onSubmit: onSubmit,
      onCancel: this.props.onCancel
    },
      e(LabelInput, {
        label: 'Name',
        value: this.state.name,
        onChange: this.handleNameChange
      })
    )
  }
}
