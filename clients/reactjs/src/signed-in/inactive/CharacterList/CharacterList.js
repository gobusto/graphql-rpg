// A list of characters.
class CharacterList extends React.Component {
  constructor (props) {
    super(props)
    this.state = { characters: null, createMode: false, errors: [] }

    this.handleNewCharacter = this.handleNewCharacter.bind(this)
    this.handleCancel = this.handleCancel.bind(this)

    this.handleCreate = this.handleCreate.bind(this)
    this.handleDestroy = this.handleDestroy.bind(this)
  }

  componentDidMount () {
    CharacterApi.all(this.props.token).then(
      result => this.setState({ characters: result.currentUser.characters }),
      errors => this.setState({ errors: errors })
    )
  }

  handleNewCharacter (event) {
    event.preventDefault()
    this.setState({ errors: [], createMode: true })
  }

  handleCancel (event) {
    event.preventDefault()
    this.setState({ errors: [], createMode: false })
  }

  handleCreate (event, params) {
    event.preventDefault()

    CharacterApi.create(this.props.token, params.name).then(
      result => this.setState({
        characters: this.state.characters.concat([result.createCharacter.character]),
        errors: [],
        createMode: false
      }),
      errors => this.setState({ errors: errors })
    )
  }

  handleDestroy (event, character) {
    event.preventDefault()
    if (!window.confirm(`Really delete ${character.name}?`)) { return }

    CharacterApi.destroy(this.props.token, character.id).then(
      result => this.setState({
        characters: this.state.characters.filter(c => c.id !== character.id)
      }),
      errors => this.setState({ errors: errors })
    )
  }

  render () {
    const e = React.createElement

    if (this.state.characters === null) {
      return null
    } else if (this.state.createMode) {
      return e(CharacterForm, {
        error: this.state.errors.join(' / '),
        onSubmit: this.handleCreate,
        onCancel: this.handleCancel
      })
    }

    const items = this.state.characters.map((character, index) => e(
      CharacterListItem, {
        key: character.name,
        character: character,
        onSelect: event => this.props.onSelect(event, character),
        onDestroy: event => this.handleDestroy(event, character)
      }
    ))

    return e(Form, {
      title: 'Characters',
      error: this.state.errors.join(' / '),
      submitText: 'Create',
      onSubmit: this.handleNewCharacter
    },
      items
    )
  }
}
