function CharacterListItem (props) {
  const e = React.createElement

  return e('div', { className: 'character-list-item' },
    e('div', { className: 'name' }, props.character.name),
    e('div', { className: 'experience' }, `${props.character.experience} XP`),
    e('div', { className: 'money' }, `$${props.character.money}`),
    e('div', { className: 'actions' },
      e(Button, { label: 'Select', onClick: props.onSelect }),
      e(Button, { label: 'Delete', onClick: props.onDestroy })
    )
  )
}
