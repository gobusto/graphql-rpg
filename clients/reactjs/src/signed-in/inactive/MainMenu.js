function MainMenu (props) {
  const e = React.createElement

  return e('div', { className: 'main-menu' },
    e(NavBar, null,
      e(Button, { label: 'Log out', onClick: props.onLogOut })
    ),
    e('div', { className: 'content' },
      e(TabGroup, { labels: ['Characters', 'Account'] },
        e(CharacterList, { token: props.token, onSelect: props.onSelectCharacter }),
        e(AccountForm, { onDestroyAccount: props.onDestroyAccount })
      )
    )
  )
}
