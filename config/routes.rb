# frozen_string_literal: true

Rails.application.routes.draw do
  scope defaults: { format: 'json' } do
    devise_for :users
    authenticate { post '/graphql', to: 'graphql#execute' }
  end
end
