# frozen_string_literal: true

# See https://github.com/waiting-for-dev/devise-jwt
class CreateInvalidatedJwts < ActiveRecord::Migration[6.0]
  def change
    create_table :invalidated_jwts do |t|
      t.string :jti, null: false
      t.datetime :exp, null: false
    end
    add_index :invalidated_jwts, :jti
  end
end
