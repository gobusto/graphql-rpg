# frozen_string_literal: true

# Create an initial (fairly simple) "character" table.
class CreateCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :characters do |t|
      t.string :name
      t.references :user, index: true

      t.timestamps null: false
    end
    add_index :characters, :name, unique: true # Character names are unique!
  end
end
