# frozen_string_literal: true

# Add some basic details to the Character model:
class AddLevelExperienceAndMoneyToCharacters < ActiveRecord::Migration[6.0]
  def change
    add_column :characters, :level, :integer, null: false, default: 1
    add_column :characters, :experience, :integer, null: false, default: 0
    add_column :characters, :money, :integer, null: false, default: 0
  end
end
