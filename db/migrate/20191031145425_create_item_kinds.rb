# frozen_string_literal: true

# This table specifies details about items it is possible to own.
class CreateItemKinds < ActiveRecord::Migration[6.0]
  def change
    create_table :item_kinds do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :item_kinds, :name, unique: true
  end
end
