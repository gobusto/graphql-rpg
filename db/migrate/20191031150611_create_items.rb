# frozen_string_literal: true

# Stores details about items posessed by a character.
class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.references :item_kind, index: true # What
      t.references :character, index: true # Whom
      t.timestamps null: false
    end
  end
end
