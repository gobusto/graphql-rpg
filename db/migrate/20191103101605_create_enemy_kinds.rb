# frozen_string_literal: true

# This table specifies details about enemies it is possible to fight.
class CreateEnemyKinds < ActiveRecord::Migration[6.0]
  def change
    create_table :enemy_kinds do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :enemy_kinds, :name, unique: true
  end
end
