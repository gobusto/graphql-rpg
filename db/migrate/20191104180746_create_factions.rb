# frozen_string_literal: true

# This table records which "factions" (AKA guilds/clans) exist.
class CreateFactions < ActiveRecord::Migration[6.0]
  def change
    create_table :factions do |t|
      t.string :name
      t.integer :membership_policy, null: false, default: 0
      t.integer :money, null: false, default: 0
      t.timestamps null: false
    end
    add_index :factions, :name, unique: true
  end
end
