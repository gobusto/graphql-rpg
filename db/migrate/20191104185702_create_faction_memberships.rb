# frozen_string_literal: true

# This table links characters to factions.
class CreateFactionMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :faction_memberships do |t|
      t.references :faction, index: true
      t.references :character, index: false
      t.integer :status, null: false # No default value, since it varies.
      t.timestamps null: false
    end
    add_index :faction_memberships, :character_id, unique: true
  end
end
