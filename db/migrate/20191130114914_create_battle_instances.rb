# frozen_string_literal: true

# This table keeps track of the state of battle instances.
class CreateBattleInstances < ActiveRecord::Migration[6.0]
  def change
    create_table :battle_instances do |t|
      t.timestamps null: false
    end
  end
end
