# frozen_string_literal: true

# This table keeps track of battle participants.
class CreateInvolvements < ActiveRecord::Migration[6.0]
  def change
    create_table :involvements do |t|
      t.references :battle
      t.references :participant, polymorphic: true
      t.integer :team

      t.timestamps
    end
  end
end
