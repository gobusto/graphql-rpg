# frozen_string_literal: true

# This table is meant to be for NPCs, so "enemies" is not the right word.
class RenameEnemyKindsToMonstersKinds < ActiveRecord::Migration[6.0]
  def change
    rename_table :enemy_kinds, :monsters_kinds
  end
end
