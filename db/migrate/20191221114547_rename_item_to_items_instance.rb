# frozen_string_literal: true

# We want items and and item kinds to live in a shared namespace:
class RenameItemToItemsInstance < ActiveRecord::Migration[6.0]
  def change
    rename_table :items, :items_instances
  end
end
