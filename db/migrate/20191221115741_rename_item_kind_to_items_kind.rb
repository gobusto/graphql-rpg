# frozen_string_literal: true

# We want items and and item kinds to live in a shared namespace:
class RenameItemKindToItemsKind < ActiveRecord::Migration[6.0]
  def change
    rename_table :item_kinds, :items_kinds
  end
end
