# frozen_string_literal: true

# Move this into a namespace.
class RenameBattleInstancesToBattlesInstances < ActiveRecord::Migration[6.0]
  def change
    rename_table :battle_instances, :battles_instances
  end
end
