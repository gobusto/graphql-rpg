# frozen_string_literal: true

# Avoid "Index name [...] is too long; the limit is 64 characters
class RenameBattleParticipantIndex < ActiveRecord::Migration[6.0]
  def change
    rename_index(
      :involvements,
      :index_involvements_on_participant_type_and_participant_id,
      :index_battles_involvements_on_participant
    )
  end
end
