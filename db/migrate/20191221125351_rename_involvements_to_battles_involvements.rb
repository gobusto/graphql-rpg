# frozen_string_literal: true

# Move this into a namespace.
class RenameInvolvementsToBattlesInvolvements < ActiveRecord::Migration[6.0]
  def change
    rename_table :involvements, :battles_involvements
  end
end
