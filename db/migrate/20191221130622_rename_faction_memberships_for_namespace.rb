# frozen_string_literal: true

# Move this into a namespace.
class RenameFactionMembershipsForNamespace < ActiveRecord::Migration[6.0]
  def change
    rename_table :faction_memberships, :factions_memberships
  end
end
