# frozen_string_literal: true

# Move this into a namespace.
class RenameFactionToFactionsInstance < ActiveRecord::Migration[6.0]
  def change
    rename_table :factions, :factions_instances
  end
end
