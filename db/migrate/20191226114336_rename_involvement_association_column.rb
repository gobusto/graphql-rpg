# frozen_string_literal: true

# Battle is now Battles::Instance, so use the expected column name.
class RenameInvolvementAssociationColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :battles_involvements, :battle_id, :instance_id
  end
end
