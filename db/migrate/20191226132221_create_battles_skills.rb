# frozen_string_literal: true

# These can be used by characters or NPC monsters, so they go in Battles:: for now.
class CreateBattlesSkills < ActiveRecord::Migration[6.0]
  def change
    create_table :battles_skills do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
