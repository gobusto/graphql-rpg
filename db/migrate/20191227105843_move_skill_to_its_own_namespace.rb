# frozen_string_literal: true

# There's going to be join models for Characters + Monsters, so...
class MoveSkillToItsOwnNamespace < ActiveRecord::Migration[6.0]
  def change
    rename_table :battles_skills, :skills_kinds
  end
end
