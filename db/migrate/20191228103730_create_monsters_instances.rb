# frozen_string_literal: true

# This is necessary for battles.
class CreateMonstersInstances < ActiveRecord::Migration[6.0]
  def change
    create_table :monsters_instances do |t|
      t.references :kind, index: true

      t.timestamps null: false
    end
  end
end
