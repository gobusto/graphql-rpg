# frozen_string_literal: true

# Simplify this now that it's in a namespace.
class RenameItemKindColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :items_instances, :item_kind_id, :kind_id
  end
end
