# frozen_string_literal: true

# This links a character or NPC to a skill they know.
class CreateSkillInstances < ActiveRecord::Migration[6.0]
  def change
    create_table :skills_instances do |t|
      t.references :kind, index: true
      t.references :caster, index: true, polymorphic: true
      t.timestamps
    end
  end
end
