# frozen_string_literal: true

# This specifies the action to be taken on a particular combat turn.
class CreateBattlesActions < ActiveRecord::Migration[6.0]
  def change
    create_table :battles_actions do |t|
      t.references :involvement, index: true
      t.references :skill, index: true
      t.boolean :resolved, null: false, default: false

      t.timestamps null: false
    end
  end
end
