# frozen_string_literal: true

# This won't be necessary for the way in which progression is intended to work.
class RemoveLevelFromCharacter < ActiveRecord::Migration[6.0]
  def change
    remove_column :characters, :level, :integer, null: false, default: 1
  end
end
