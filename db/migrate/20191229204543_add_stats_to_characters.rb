# frozen_string_literal: true

# Character and NPC stats.
class AddStatsToCharacters < ActiveRecord::Migration[6.0]
  def change
    %i[strength speed vitality magic faith personality].each do |field|
      %i[characters monsters_kinds].each do |table|
        add_column table, field, :integer, null: false, default: 0
      end
    end
  end
end
