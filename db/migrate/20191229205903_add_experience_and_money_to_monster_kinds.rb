# frozen_string_literal: true

# Similar to characters, but these indicate rewards for defeating an NPC.
class AddExperienceAndMoneyToMonsterKinds < ActiveRecord::Migration[6.0]
  def change
    add_column :monsters_kinds, :experience, :integer, null: false, default: 0
    add_column :monsters_kinds, :money, :integer, null: false, default: 0
  end
end
