# frozen_string_literal: true

# This isn't unique...
class RemoveNonUniqueParticipantIndex < ActiveRecord::Migration[6.0]
  def change
    remove_index(
      :battles_involvements,
      column: %i[participant_type participant_id],
      name: :index_battles_involvements_on_participant
    )
  end
end
