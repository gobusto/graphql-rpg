# frozen_string_literal: true

# ...now it is.
class AddUniqueParticipantIndex < ActiveRecord::Migration[6.0]
  def change
    add_index(
      :battles_involvements,
      %i[participant_type participant_id],
      name: :index_battles_involvements_on_participant,
      unique: true
    )
  end
end
