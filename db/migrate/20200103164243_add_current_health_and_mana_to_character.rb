# frozen_string_literal: true

# This applies to characters and monster INSTANCES (but not "kinds").
class AddCurrentHealthAndManaToCharacter < ActiveRecord::Migration[6.0]
  def change
    %i[current_health current_mana].each do |field|
      %i[characters monsters_instances].each do |table|
        add_column table, field, :integer, null: false, default: 0
      end
    end
  end
end
