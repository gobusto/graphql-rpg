# frozen_string_literal: true

FactoryBot.define do
  factory :battle_action, class: 'Battles::Action' do
    association :involvement, factory: :battle_involvement, strategy: :build

    after(:build) do |model|
      caster = model.involvement.participant # Characters have skills directly.
      caster = caster.kind if caster.is_a?(Monsters::Instance) # NPCs don't.
      model.skill ||= build(:skill_instance, caster: caster)
    end
  end
end
