# frozen_string_literal: true

FactoryBot.define do
  factory :battle_instance, class: 'Battles::Instance' do
    after(:build) do |instance|
      instance.involvements << build(:battle_involvement, instance: instance)
      instance.involvements << build(:battle_involvement, instance: instance)
    end
  end
end
