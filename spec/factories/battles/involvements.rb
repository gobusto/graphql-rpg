# frozen_string_literal: true

FactoryBot.define do
  factory :battle_involvement, class: 'Battles::Involvement' do
    association :participant, factory: :character, strategy: :build

    after(:build) do |model|
      model.instance ||= build(:battle_instance, involvements: [model])
    end
  end
end
