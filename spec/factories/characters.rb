# frozen_string_literal: true

FactoryBot.define do
  factory :character do
    association :user, strategy: :build
    sequence(:name) { |n| "Character #{n}" }
  end
end
