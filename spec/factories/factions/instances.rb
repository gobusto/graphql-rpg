# frozen_string_literal: true

FactoryBot.define do
  factory :faction, class: 'Factions::Instance' do
    sequence(:name) { |n| "Faction #{n}" }
  end
end
