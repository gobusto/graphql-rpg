# frozen_string_literal: true

FactoryBot.define do
  factory :faction_membership, class: 'Factions::Membership' do
    association :character, strategy: :build
    association :faction, strategy: :build
    status { 'leader' }
  end
end
