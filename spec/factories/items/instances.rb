# frozen_string_literal: true

FactoryBot.define do
  factory :item_instance, class: 'Items::Instance' do
    association :character, strategy: :build
    association :kind, factory: :item_kind, strategy: :build
  end
end
