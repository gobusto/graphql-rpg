# frozen_string_literal: true

FactoryBot.define do
  factory :item_kind, class: 'Items::Kind' do
    sequence(:name) { |n| "Item #{n}" }
  end
end
