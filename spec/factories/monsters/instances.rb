# frozen_string_literal: true

FactoryBot.define do
  factory :monster_instance, class: 'Monsters::Instance' do
    association :kind, factory: :monster_kind, strategy: :build
  end
end
