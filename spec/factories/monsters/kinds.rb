# frozen_string_literal: true

FactoryBot.define do
  factory :monster_kind, class: 'Monsters::Kind' do
    sequence(:name) { |n| "Monster #{n}" }
  end
end
