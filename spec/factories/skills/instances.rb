# frozen_string_literal: true

FactoryBot.define do
  factory :skill_instance, class: 'Skills::Instance' do
    association :kind, factory: :skill_kind, strategy: :build
    association :caster, factory: :monster_kind, strategy: :build
  end
end
