# frozen_string_literal: true

FactoryBot.define do
  factory :skill_kind, class: 'Skills::Kind' do
    sequence(:name) { |n| "Skill #{n}" }
  end
end
