# frozen_string_literal: true

RSpec.describe 'Battle Action', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      battle_action = build(:battle_action)
      battle_action.valid?
      expect(battle_action.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        'Involvement must exist',
        'Skill must exist'
      ]

      battle_action = Battles::Action.new
      battle_action.valid?
      expect(battle_action.errors.full_messages).to eq(messages)
    end
  end
end
