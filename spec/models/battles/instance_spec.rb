# frozen_string_literal: true

RSpec.describe 'Battle instance', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      battle_instance = build(:battle_instance)
      battle_instance.valid?
      expect(battle_instance.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        'Team count must be greater than 1'
      ]

      battle_instance = Battles::Instance.new
      battle_instance.valid?
      expect(battle_instance.errors.full_messages).to eq(messages)
    end
  end
end
