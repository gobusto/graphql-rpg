# frozen_string_literal: true

RSpec.describe 'Battle involvement', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      involvement = build(:battle_involvement)
      involvement.valid?
      expect(involvement.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        'Instance must exist',
        'Participant must exist',
        'Participant type is not included in the list'
      ]

      involvement = Battles::Involvement.new
      involvement.valid?
      expect(involvement.errors.full_messages).to eq(messages)
    end
  end

  context 'participant type value' do
    it 'can refer to a character' do
      involvement = Battles::Involvement.new(
        instance: Battles::Instance.new,
        participant: Character.new
      )

      involvement.valid?
      expect(involvement.errors.full_messages).to eq([])
    end

    it 'can refer to a monster instance' do
      involvement = Battles::Involvement.new(
        instance: Battles::Instance.new,
        participant: Monsters::Instance.new(kind: Monsters::Kind.new)
      )

      involvement.valid?
      expect(involvement.errors.full_messages).to eq([])
    end

    it 'becomes a new monster instance if a monster type is specified' do
      involvement = Battles::Involvement.new(
        instance: Battles::Instance.new,
        participant: Monsters::Kind.new(name: 'Troll')
      )

      involvement.valid?
      expect(involvement.errors.full_messages).to eq([])

      expect(involvement.participant.class).to eq(Monsters::Instance)
      expect(involvement.participant.kind.name).to eq('Troll')
    end

    it 'cannot refer to anything unexpected' do
      error = 'Participant type is not included in the list'

      involvement = Battles::Involvement.new(
        instance: Battles::Instance.new,
        participant: User.new
      )

      involvement.valid?
      expect(involvement.errors.full_messages).to eq([error])
    end
  end
end
