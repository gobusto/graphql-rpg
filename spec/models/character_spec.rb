# frozen_string_literal: true

RSpec.describe 'Character', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      character = build(:character)
      character.valid?
      expect(character.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        "Name can't be blank",
        'User must exist'
      ]

      character = Character.new
      character.valid?
      expect(character.errors.full_messages).to eq(messages)
    end

    it 'fails if the name is not unique' do
      messages = [
        'Name has already been taken'
      ]

      create(:character, name: 'Steve')

      character = build(:character, name: 'Steve')
      character.valid?
      expect(character.errors.full_messages).to eq(messages)
    end
  end
end
