# frozen_string_literal: true

RSpec.describe 'Faction', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      faction = build(:faction)
      faction.valid?
      expect(faction.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        "Name can't be blank"
      ]

      faction = Factions::Instance.new
      faction.valid?
      expect(faction.errors.full_messages).to eq(messages)
    end

    it 'fails if the name is not unique' do
      messages = [
        'Name has already been taken'
      ]

      create(:faction, name: 'The Good Guys')

      faction = build(:faction, name: 'The Good Guys')
      faction.valid?
      expect(faction.errors.full_messages).to eq(messages)
    end
  end
end
