# frozen_string_literal: true

RSpec.describe 'Faction Membership', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      faction_membership = build(:faction_membership)
      faction_membership.valid?
      expect(faction_membership.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        'Character must exist',
        'Faction must exist',
        "Status can't be blank"
      ]

      faction_membership = Factions::Membership.new
      faction_membership.valid?
      expect(faction_membership.errors.full_messages).to eq(messages)
    end

    it 'fails if the character is not unique' do
      messages = [
        'Character has already been taken'
      ]

      character = create(:faction_membership).character

      faction_membership = build(:faction_membership, character: character)
      faction_membership.valid?
      expect(faction_membership.errors.full_messages).to eq(messages)
    end
  end
end
