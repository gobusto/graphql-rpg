# frozen_string_literal: true

RSpec.describe 'Items: Instance', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      item = build(:item_instance)
      item.valid?
      expect(item.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        'Character must exist',
        'Kind must exist'
      ]

      item = Items::Instance.new
      item.valid?
      expect(item.errors.full_messages).to eq(messages)
    end
  end
end
