# frozen_string_literal: true

RSpec.describe 'Item Kind', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      item_kind = build(:item_kind)
      item_kind.valid?
      expect(item_kind.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        "Name can't be blank"
      ]

      item_kind = Items::Kind.new
      item_kind.valid?
      expect(item_kind.errors.full_messages).to eq(messages)
    end

    it 'fails if the name is not unique' do
      messages = [
        'Name has already been taken'
      ]

      create(:item_kind, name: 'Scroll')

      item_kind = build(:item_kind, name: 'Scroll')
      item_kind.valid?
      expect(item_kind.errors.full_messages).to eq(messages)
    end
  end
end
