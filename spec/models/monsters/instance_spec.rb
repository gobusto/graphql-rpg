# frozen_string_literal: true

RSpec.describe 'Monsters: Instance', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      monster_instance = build(:monster_instance)
      monster_instance.valid?
      expect(monster_instance.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        'Kind must exist'
      ]

      monster_instance = Monsters::Instance.new
      monster_instance.valid?
      expect(monster_instance.errors.full_messages).to eq(messages)
    end
  end
end
