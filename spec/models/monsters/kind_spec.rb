# frozen_string_literal: true

RSpec.describe 'Monsters: Kind', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      monster_kind = build(:monster_kind)
      monster_kind.valid?
      expect(monster_kind.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        "Name can't be blank"
      ]

      monster_kind = Monsters::Kind.new
      monster_kind.valid?
      expect(monster_kind.errors.full_messages).to eq(messages)
    end

    it 'fails if the name is not unique' do
      messages = [
        'Name has already been taken'
      ]

      create(:monster_kind, name: 'Troll')

      monster_kind = build(:monster_kind, name: 'Troll')
      monster_kind.valid?
      expect(monster_kind.errors.full_messages).to eq(messages)
    end
  end
end
