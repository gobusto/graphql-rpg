# frozen_string_literal: true

RSpec.describe 'Skill instance', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      skill = build(:skill_instance)
      skill.valid?
      expect(skill.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        'Kind must exist',
        'Caster must exist',
        'Caster type is not included in the list'
      ]

      skill = Skills::Instance.new
      skill.valid?
      expect(skill.errors.full_messages).to eq(messages)
    end
  end

  context 'caster type value' do
    it 'can refer to a character' do
      skill = Skills::Instance.new(
        kind: Skills::Kind.new,
        caster: Character.new
      )

      skill.valid?
      expect(skill.errors.full_messages).to eq([])
    end

    it 'can refer to a monster kind' do
      skill = Skills::Instance.new(
        kind: Skills::Kind.new,
        caster: Monsters::Kind.new
      )

      skill.valid?
      expect(skill.errors.full_messages).to eq([])
    end

    it 'cannot refer to anything unexpected' do
      error = 'Caster type is not included in the list'

      skill = Skills::Instance.new(
        kind: Skills::Kind.new,
        caster: User.new
      )

      skill.valid?
      expect(skill.errors.full_messages).to eq([error])
    end
  end
end
