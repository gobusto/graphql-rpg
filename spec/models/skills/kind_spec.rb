# frozen_string_literal: true

RSpec.describe 'Skill', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      skill = build(:skill_kind)
      skill.valid?
      expect(skill.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = [
        "Name can't be blank"
      ]

      skill = Skills::Kind.new
      skill.valid?
      expect(skill.errors.full_messages).to eq(messages)
    end

    it 'fails if the name is not unique' do
      messages = [
        'Name has already been taken'
      ]

      create(:skill_kind, name: 'Defend')

      skill = build(:skill_kind, name: 'Defend')
      skill.valid?
      expect(skill.errors.full_messages).to eq(messages)
    end
  end
end
