# frozen_string_literal: true

RSpec.describe 'User', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      user = build(:user)
      user.valid?
      expect(user.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      messages = ["Email can't be blank", "Password can't be blank"]

      user = User.new
      expect(user.valid?).to eq(false)
      expect(user.errors.full_messages).to eq(messages)
    end
  end
end
