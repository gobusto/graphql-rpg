# frozen_string_literal: true

RSpec.describe 'Mutation: Start Battle', type: :request do
  RSPEC_START_BATTLE = %<
    mutation StartBattle($involvements: [InvolvementAttributes!]!) {
      startBattle(involvements: $involvements) {
        errors,
        battle {
          involvements {
            team
            participant {
              modelName
              ... on Character {
                name
              }
              ... on Monster {
                kind {
                  name
                }
              }
            }
            ready
          }
        }
      }
    }
  >

  context 'succeeds when it' do
    it 'specifies a mixture of characters and monsters' do
      monster = create(:monster_kind, name: 'Slime')
      character = create(:character, name: 'Steve')
      sign_in(character.user)

      expected_json = {
        'data' => {
          'startBattle' => {
            'errors' => [],
            'battle' => {
              'involvements' => [
                {
                  'team' => nil,
                  'participant' => {
                    'modelName' => 'Character',
                    'name' => 'Steve'
                  },
                  'ready' => false
                },
                {
                  'team' => 'RED',
                  'participant' => {
                    'modelName' => 'Monsters::Instance',
                    'kind' => {
                      'name' => 'Slime'
                    }
                  },
                  'ready' => false
                }
              ]
            }
          }
        }
      }

      data = {
        'involvements' => [
          {
            'participantType' => 'CHARACTER',
            'participantId' => character.id.to_s
          },
          {
            'participantType' => 'MONSTER',
            'participantId' => monster.id.to_s,
            'team' => 'RED'
          }
        ]
      }

      params = { query: RSPEC_START_BATTLE, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'specifies only characters' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  # NOTE: A lot of these could just be model specs, rather than GraphQL ones.
  context 'fails when it' do
    it 'specifies zero participants' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'specifies only one participant' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'specifies only monsters' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked on a character who is already involved in a battle' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'tries to assign all participants to a single team' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
