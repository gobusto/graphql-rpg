# frozen_string_literal: true

RSpec.describe 'Mutation: Create Character', type: :request do
  before(:each) do
    sign_in(create(:user))
  end

  RSPEC_CREATE_CHARACTER = %<
    mutation CreateCharacter($attributes: CharacterAttributes!) {
      createCharacter(attributes: $attributes) {
        errors,
        character {
          name
        }
      }
    }
  >

  it 'succeeds if the provided parameters are valid' do
    expected_json = {
      'data' => {
        'createCharacter' => {
          'errors' => [],
          'character' => {
            'name' => 'Steve'
          }
        }
      }
    }

    data = {
      'attributes' => {
        'name' => 'Steve'
      }
    }

    params = { query: RSPEC_CREATE_CHARACTER, variables: data.to_json }
    post '/graphql', params: params
    expect(JSON.parse(response.body)).to eq(expected_json)
  end

  it 'fails if the name is not unique' do
    expected_json = {
      'data' => {
        'createCharacter' => {
          'errors' => [
            'Name has already been taken'
          ],
          'character' => nil
        }
      }
    }

    create(:character, name: 'Steve')

    data = {
      'attributes' => {
        'name' => 'Steve'
      }
    }

    params = { query: RSPEC_CREATE_CHARACTER, variables: data.to_json }
    post '/graphql', params: params
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
