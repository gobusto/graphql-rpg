# frozen_string_literal: true

RSpec.describe 'Mutation: Create Character', type: :request do
  RSPEC_DESTROY_CHARACTER = %<
    mutation DestroyCharacter($id: ID!) {
      destroyCharacter(id: $id) {
        errors,
        character {
          name
        }
      }
    }
  >

  it 'succeeds if the provided parameters are valid' do
    user = create(:user)
    sign_in(user)

    character = create(:character, name: 'Steve', user: user)

    expected_json = {
      'data' => {
        'destroyCharacter' => {
          'errors' => [],
          'character' => {
            'name' => 'Steve'
          }
        }
      }
    }

    data = { 'id' => character.id }

    params = { query: RSPEC_DESTROY_CHARACTER, variables: data.to_json }
    post '/graphql', params: params
    expect(JSON.parse(response.body)).to eq(expected_json)
  end

  it 'fails if the character does not exist' do
    user = create(:user)
    sign_in(user)

    expected_json = {
      'data' => {
        'destroyCharacter' => {
          'errors' => ['Record not found'],
          'character' => nil
        }
      }
    }

    data = { 'id' => 1 }

    params = { query: RSPEC_DESTROY_CHARACTER, variables: data.to_json }
    post '/graphql', params: params
    expect(JSON.parse(response.body)).to eq(expected_json)
  end

  it 'fails if the character belongs to someone else' do
    user = create(:user)
    sign_in(user)

    character = create(:character, name: 'Steve')

    expected_json = {
      'data' => {
        'destroyCharacter' => {
          'errors' => ['Record not found'],
          'character' => nil
        }
      }
    }

    data = { 'id' => character.id }

    params = { query: RSPEC_DESTROY_CHARACTER, variables: data.to_json }
    post '/graphql', params: params
    expect(JSON.parse(response.body)).to eq(expected_json)
  end

  it 'fails if the character cannot be destroyed' do
    pending 'TODO'
    raise NotImplementedError, 'TODO'
  end
end
