# frozen_string_literal: true

RSpec.describe 'Mutation: Create Faction Membership', type: :request do
  RSPEC_CREATE_MEMBERSHIP = %<
    mutation CreateFactionMembership($factionId: ID!, $characterId: ID!) {
      createFactionMembership(factionId:$factionId, characterId:$characterId) {
        errors,
        factionMembership {
          status
        }
      }
    }
  >

  context 'succeeds when it' do
    it 'specifies an approval-based-membership faction' do
      faction = create(:faction, membership_policy: :approval_based)

      character = create(:character)
      sign_in(character.user)

      expected_json = {
        'data' => {
          'createFactionMembership' => {
            'errors' => [],
            'factionMembership' => {
              'status' => 'PENDING'
            }
          }
        }
      }

      data = { 'factionId' => faction.id, 'characterId' => character.id }

      params = { query: RSPEC_CREATE_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'specifies an unrestricted-membership faction' do
      faction = create(:faction, membership_policy: :unrestricted)

      character = create(:character)
      sign_in(character.user)

      expected_json = {
        'data' => {
          'createFactionMembership' => {
            'errors' => [],
            'factionMembership' => {
              'status' => 'STANDARD'
            }
          }
        }
      }

      data = { 'factionId' => faction.id, 'characterId' => character.id }

      params = { query: RSPEC_CREATE_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end
  end

  context 'fails when it' do
    it 'specifies an invite-only-membership faction' do
      faction = create(:faction, membership_policy: :invite_only)

      character = create(:character)
      sign_in(character.user)

      expected_json = {
        'data' => {
          'createFactionMembership' => {
            'errors' => ["Status can't be blank"],
            'factionMembership' => nil
          }
        }
      }

      data = { 'factionId' => faction.id, 'characterId' => character.id }

      params = { query: RSPEC_CREATE_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'specifies an invalid faction ID' do
      character = create(:character)
      sign_in(character.user)

      expected_json = {
        'data' => {
          'createFactionMembership' => {
            'errors' => ['Faction must exist', "Status can't be blank"],
            'factionMembership' => nil
          }
        }
      }

      data = { 'factionId' => 123, 'characterId' => character.id }

      params = { query: RSPEC_CREATE_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'is invoked when the current user has no active character' do
      faction = create(:faction, membership_policy: :approval_based)

      sign_in(create(:user))

      expected_json = {
        'data' => {
          'createFactionMembership' => {
            'errors' => ['Character must exist'],
            'factionMembership' => nil
          }
        }
      }

      data = { 'factionId' => faction.id, 'characterId' => 123 }

      params = { query: RSPEC_CREATE_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'is invoked when the active character is already a faction member' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
