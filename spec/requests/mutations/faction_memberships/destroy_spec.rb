# frozen_string_literal: true

RSpec.describe 'Mutation: Destroy Faction Membership', type: :request do
  RSPEC_DESTROY_MEMBERSHIP = %<
    mutation DestroyfactionMembership($id: ID!) {
      destroyFactionMembership(id: $id) {
        errors,
        character {
          id
        }
      }
    }
  >

  context 'succeeds when it' do
    it 'is called by a "pending" member' do
      faction = create(:faction_membership, status: :leader).faction

      member = create(:faction_membership, status: :pending, faction: faction)
      sign_in(member.character.user)

      expected_json = {
        'data' => {
          'destroyFactionMembership' => {
            'errors' => [],
            'character' => {
              'id' => member.character_id.to_s
            }
          }
        }
      }

      data = { 'id' => member.id }

      params = { query: RSPEC_DESTROY_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'is called by a "standard" member' do
      faction = create(:faction_membership, status: :leader).faction

      member = create(:faction_membership, status: :standard, faction: faction)
      sign_in(member.character.user)

      expected_json = {
        'data' => {
          'destroyFactionMembership' => {
            'errors' => [],
            'character' => {
              'id' => member.character_id.to_s
            }
          }
        }
      }

      data = { 'id' => member.id }

      params = { query: RSPEC_DESTROY_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'is called by a "leader" member and at least one other leader exists' do
      faction = create(:faction_membership, status: :leader).faction

      member = create(:faction_membership, status: :leader, faction: faction)
      sign_in(member.character.user)

      expected_json = {
        'data' => {
          'destroyFactionMembership' => {
            'errors' => [],
            'character' => {
              'id' => member.character_id.to_s
            }
          }
        }
      }

      data = { 'id' => member.id }

      params = { query: RSPEC_DESTROY_MEMBERSHIP, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end
  end

  context 'fails when it' do
    it 'is invoked without an active character' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked for an active character without a faction membership' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked by a leader when no other leaders exist' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'fails to delete the membership from the database' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
