# frozen_string_literal: true

RSpec.describe 'Mutation: Update Faction Membership', type: :request do
  RSPEC_UPDATE_MEMBERSHIP = %<
    mutation UpdateFactionMembership($id: ID!, $status: MembershipStatus!) {
      updateFactionMembership(id: $id, status: $status) {
        errors,
        factionMembership {
          status
        }
      }
    }
  >

  it 'needs test coverage' do
    pending 'TODO'
    raise NotImplementedError, 'TODO'
  end
end
