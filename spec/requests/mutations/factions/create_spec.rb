# frozen_string_literal: true

RSpec.describe 'Mutation: Create Faction', type: :request do
  RSPEC_CREATE_FACTION = %<
    mutation CreateFaction($characterId: ID!, $attributes: FactionAttributes!) {
      createFaction(characterId: $characterId, attributes: $attributes) {
        errors,
        faction {
          name,
          membershipPolicy,
          money,
          factionMemberships {
            status,
            character {
              id
            }
          }
        }
      }
    }
  >

  context 'succeeds when it' do
    it 'specifies everything explicitly' do
      character = create(:character)
      sign_in(character.user)

      expected_json = {
        'data' => {
          'createFaction' => {
            'errors' => [],
            'faction' => {
              'name' => 'Cool and the Gang',
              'membershipPolicy' => 'UNRESTRICTED',
              'money' => 0,
              'factionMemberships' => [
                {
                  'status' => 'LEADER',
                  'character' => {
                    'id' => character.id.to_s
                  }
                }
              ]
            }
          }
        }
      }

      data = {
        'characterId' => character.id.to_s,
        'attributes' => {
          'name' => 'Cool and the Gang',
          'membershipPolicy' => 'UNRESTRICTED'
        }
      }

      params = { query: RSPEC_CREATE_FACTION, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'only specifies a name' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'fails when it' do
    it 'specifies an existing faction name' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked when the current user has no active character' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked when the active character already belongs to a faction' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
