# frozen_string_literal: true

RSpec.describe 'Mutation: Update Faction', type: :request do
  RSPEC_UPDATE_FACTION = %<
    mutation UpdateFaction($id: ID!, $attributes: FactionAttributes!) {
      updateFaction(id: $id, attributes: $attributes) {
        errors,
        faction {
          name,
          membershipPolicy
        }
      }
    }
  >

  context 'succeeds when it' do
    it 'specifies everything explicitly' do
      character = create(:faction_membership).character
      sign_in(character.user)

      expected_json = {
        'data' => {
          'updateFaction' => {
            'errors' => [],
            'faction' => {
              'name' => 'Cool and the Gang',
              'membershipPolicy' => 'UNRESTRICTED'
            }
          }
        }
      }

      data = {
        'id' => character.faction_membership.faction_id.to_s,
        'attributes' => {
          'name' => 'Cool and the Gang',
          'membershipPolicy' => 'UNRESTRICTED'
        }
      }

      params = { query: RSPEC_UPDATE_FACTION, variables: data.to_json }
      post '/graphql', params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'only specifies a name' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'only specifies a membership policy' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'fails when it' do
    it 'specifies an existing faction name' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked when the current user has no active character' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked when the active character has no faction' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked when the active character is a standard member' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is invoked when the active character is a pending member' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
