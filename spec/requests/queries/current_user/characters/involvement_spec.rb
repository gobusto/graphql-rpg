# frozen_string_literal: true

RSpec.describe 'Query: Current User - Character Involvement', type: :request do
  it 'can be queried' do
    character = create(:character, name: 'Steve')
    create(:battle_involvement, participant: character, team: 'red')
    sign_in(character.user)

    query = %(
      {
        currentUser {
          characters {
            involvement {
              team
            }
          }
        }
      }
    )

    expected_json = {
      'data' => {
        'currentUser' => {
          'characters' => [
            {
              'involvement' => {
                'team' => 'RED'
              }
            }
          ]
        }
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
