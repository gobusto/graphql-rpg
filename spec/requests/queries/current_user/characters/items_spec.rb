# frozen_string_literal: true

RSpec.describe 'Query: Current User - Character Items', type: :request do
  it 'can be queried' do
    character = create(:character, name: 'Steve')
    item = create(:item_kind, name: 'Potion')
    create(:item_instance, kind: item, character: character)
    sign_in(character.user)

    query = %(
      {
        currentUser {
          characters {
            items {
              kind {
                name
              }
            }
          }
        }
      }
    )

    expected_json = {
      'data' => {
        'currentUser' => {
          'characters' => [
            {
              'items' => [
                {
                  'kind' => {
                    'name' => 'Potion'
                  }
                }
              ]
            }
          ]
        }
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
