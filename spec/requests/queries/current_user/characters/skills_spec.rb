# frozen_string_literal: true

RSpec.describe 'Query: Current User - Character Skills', type: :request do
  it 'can be queried' do
    character = create(:character, name: 'Steve')
    skill = create(:skill_kind, name: 'Defend')
    create(:skill_instance, kind: skill, caster: character)
    sign_in(character.user)

    query = %(
      {
        currentUser {
          characters {
            skills {
              kind {
                name
              }
            }
          }
        }
      }
    )

    expected_json = {
      'data' => {
        'currentUser' => {
          'characters' => [
            {
              'skills' => [
                {
                  'kind' => {
                    'name' => 'Defend'
                  }
                }
              ]
            }
          ]
        }
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
