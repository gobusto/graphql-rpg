# frozen_string_literal: true

RSpec.describe 'Query: Current User - Characters', type: :request do
  it 'can be queried' do
    character = create(:character, name: 'Steve')
    sign_in(character.user)

    query = %(
      {
        currentUser {
          characters {
            id
            name
            currentHealth
            maximumHealth
            currentMana
            maximumMana
            strength
            speed
            vitality
            magic
            faith
            personality
            experience
            money
          }
        }
      }
    )

    expected_json = {
      'data' => {
        'currentUser' => {
          'characters' => [
            {
              'id' => character.id.to_s,
              'name' => 'Steve',
              'currentHealth' => 10,
              'maximumHealth' => 10,
              'currentMana' => 0,
              'maximumMana' => 0,
              'strength' => 0,
              'speed' => 0,
              'vitality' => 0,
              'magic' => 0,
              'faith' => 0,
              'personality' => 0,
              'experience' => 0,
              'money' => 0
            }
          ]
        }
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
