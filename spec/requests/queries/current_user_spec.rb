# frozen_string_literal: true

RSpec.describe 'Query: Current User', type: :request do
  it 'can be queried' do
    user = create(:user, email: 'test@example.com')
    sign_in(user)

    query = %(
      {
        currentUser {
          id,
          email
        }
      }
    )

    expected_json = {
      'data' => {
        'currentUser' => {
          'id' => user.id.to_s,
          'email' => 'test@example.com'
        }
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
