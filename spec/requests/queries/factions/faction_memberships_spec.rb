# frozen_string_literal: true

RSpec.describe 'Query: Factions - Memberships', type: :request do
  it 'can be queried' do
    faction_membership = create(:faction_membership, status: :leader)
    sign_in(faction_membership.character.user)

    query = %(
      {
        factions {
          factionMemberships {
            status,
            character {
              id
            }
          }
        }
      }
    )

    expected_json = {
      'data' => {
        'factions' => [
          {
            'factionMemberships' => [
              {
                'status' => 'LEADER',
                'character' => {
                  'id' => faction_membership.character_id.to_s
                }
              }
            ]
          }
        ]
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
