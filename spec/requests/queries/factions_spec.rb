# frozen_string_literal: true

RSpec.describe 'Query: Factions', type: :request do
  it 'can be queried' do
    sign_in(create(:user))

    create(:faction, name: 'Red', money: 123, membership_policy: :invite_only)

    query = %(
      {
        factions {
          name,
          money,
          membershipPolicy
        }
      }
    )

    expected_json = {
      'data' => {
        'factions' => [
          {
            'name' => 'Red',
            'money' => 123,
            'membershipPolicy' => 'INVITE_ONLY'
          }
        ]
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
