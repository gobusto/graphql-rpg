# frozen_string_literal: true

RSpec.describe 'Query: Items', type: :request do
  it 'can be queried' do
    sign_in(create(:user))

    create(:item_kind, name: 'Potion')

    query = %(
      {
        items {
          name
        }
      }
    )

    expected_json = {
      'data' => {
        'items' => [
          {
            'name' => 'Potion'
          }
        ]
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
