# frozen_string_literal: true

RSpec.describe 'Query: Monsters', type: :request do
  it 'can be queried' do
    sign_in(create(:user))

    create(:monster_kind, name: 'Slime')

    query = %(
      {
        monsters {
          name
        }
      }
    )

    expected_json = {
      'data' => {
        'monsters' => [
          {
            'name' => 'Slime'
          }
        ]
      }
    }

    post '/graphql', params: { query: query }
    expect(JSON.parse(response.body)).to eq(expected_json)
  end
end
